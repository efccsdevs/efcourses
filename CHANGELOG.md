# EF Courses Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.6 - 2019-07-31
### Add
- Add Market List

## 0.1.5 - 2019-07-25
### Fix
- getCoursesFrontEnd in ILS

## 0.1.4 - 2019-07-24
### Add
- getDestinationCode, getLocationFromID, courseTypesList variable function

## 0.1.3 - 2019-07-15
### Add
- getDestionationCourses function
- getAvailableLocations function

## 0.1.2 - 2019-07-03
### Add
- Add getPaymentEntry function

## 0.1.1.1 - 2019-07-03
### Fix
- remove var_dump in getDestinationsJSON

## 0.1.1 - 2019-07-03
### Added
- getDestinationsJSON for Personalised offer

## 0.1.0 - 2019-06-28
### Added
- Payment functionality for booking


## 0.0.1 - 2019-01-30
### Added
- Initial release
