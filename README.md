# EF Courses plugin for Craft CMS 3.x

Plugin to scrape EF Courses to use on the website, and take care of the payments for the booking form

## Requirements

This plugin requires Craft CMS 3.1 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /ef-courses

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for EF Courses.

## EF Courses Overview

-Insert text here-

## Configuring EF Courses

-Insert text here-

## Using EF Courses

-Insert text here-

## EF Courses Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [EF Global Creative](https://ef.design)
