<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\variables;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\elements\Category as Category;
use craft\helpers\UrlHelper;

use ef\efcourses\records\City;
use ef\efcourses\records\CourseType;
use ef\efcourses\records\ProductCode;
use ef\efcourses\records\Course;
use ef\efcourses\records\Market;

/**
 * EF Courses Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.efCourses }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class EfCoursesVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.efCourses.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.efCourses.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */

    public function locationsList($productCode)
    {
        $pcId = $this->getProductCodeId($productCode);

        $cities = City::find()
            ->select('{{ef_cities}}.*')
            ->leftJoin('{{ef_cityfields}} as cf', 'cf.cityId = {{ef_cities}}.id')
            ->leftJoin('{{ef_coursetypes}} as ct', 'ct.productCodeId = cf.productCodeId')
            ->where(['cf.productCodeId' => $pcId])
            ->with([
                'country',
                'courses',
                'cityFields' => function ($query) use ($pcId) {
                    $query->where(['productCodeId' => $pcId]);
                    //->andWhere(['enable'=>true]);
                }])
            ->orderBy("countryId ASC, cityName ASC, ")
            ->asArray()
            ->all();

        return $cities;
    }

    public function locationsListAll()
    {
        $cities = City::find()
            ->with(['country', 'courses', 'cityFields'])
            ->orderBy('cityName asc')
            ->asArray()
            ->all();

        return $cities;
    }

    public function catLocationList()
    {

        $categories = Category::find()
            ->group('destinations')
            ->level('3')
            ->orderBy('title asc')
            ->all();

        $catsArr = ['0' => 'None'];
        foreach ($categories as $category) {
            $catsArr[$category->id] = $category->title . ", " . $category->parent->title;
        }

        return $catsArr;
    }

    public function getRelationLocationID($id)
    {

        $categories = Category::find()
            ->group('destinations')
            ->level('3')
            ->orderBy('title asc')
            ->all();


        foreach ($categories as $category) {
            if (isset($category->efCity)) {
                $efCity = unserialize($category->efCity);
                if (is_array($efCity) && count($efCity) && in_array($id, $efCity)) {
                    return $category->id;
                }
            }
        }

        return 0;
    }

    public function getProductCodeId($pc)
    {
        $product = ProductCode::findOne(['productCode' => $pc]);
        if ($product === null) {
            return false;
        }
        return $product->id;
    }

    public function ilsLocationCoursesList($productcode, $destid)
    {
        // Fetch product code ID
        $pc = EF::$plugin->efCoursesShared->getProductId($productcode);

        $city = City::findOne(['id' => $destid]);

        $courses = Course::find()
            ->leftJoin('{{ef_coursetypes}} as ct', 'ct.id = {{ef_courses}}.courseTypeId')
            ->where(['cityId' => $destid])
            ->andWhere(['productCodeId' => $pc])
            ->with('courseType')
            ->all();

        return ['courses' => $courses, 'city' => $city];
    }


    public function courseList($productCode)
    {
        $courseTypesArr = EF::$plugin->efCoursesShared->getProductId($productCode);

        $courses = CourseType::find()
            ->where(['productCodeId' => $courseTypesArr])
            ->with('fields')
            ->all();

        return $courses;
    }


    public function getDestinationsJSON($productCode = false)
    {
        return EF::$plugin->efCoursesShared->getDestinationsJSON($productCode);
    }

    /**
     *
     *  Get Destination Minimum Price
     *
     *  Needs cityID, CourseID, productCodeID
     *
     */

    public function getDestinationMinPrice($entry)
    {
        return EF::$plugin->efCoursesShared->getDestinationMinPrice($entry);
    }

    public function getPaymentEntry($n)
    {
        if (!$n)
            return false;

        $n = $this->explosionKeys(explode('&', parse_url(craft()->request->getUrl(), PHP_URL_QUERY)), '=');

        $nDecode = base64_decode($n['n']);

        if (!$nDecode)
            return false;

        $exploded = $this->explosionKeys(explode('&', $nDecode), '=');

        if (!isset($exploded['entry']))
            return false;

        return $exploded;
    }

    /**
     *
     *  Get Destination Courses
     *
     *  Needs cityID, CourseID, productCodeID
     *
     *  TODO: Some shared code with the previous function could be moved to a separate function
     *
     */

    public function getDestinationCourses($entry)
    {
        return EF::$plugin->efCoursesShared->getDestinationCourses($entry);
    }

    public function getAvailableLocations($entry)
    {
        return EF::$plugin->efCoursesShared->getAvailableLocations($entry);
    }

    public function getDestinationCode($entry, $simpleCodes = false)
    {
        return EF::$plugin->efCoursesShared->getDestinationCode($entry, $simpleCodes);
    }

    public function getLocationFromID($id)
    {
        $city = Cities::find()
            ->where(['id' => $id])
            ->one();

        if (isset($city->cityName)){
            return $city->cityName;
        }

        return false;
    }

    public function ilsCoursesList($productCode)
    {
        $courseTypesArr = $this->getProductId($productCode);

        $courses = CourseType::find()
            ->where(['IN', productCodeId, $courseTypesArr])
            ->with('fields')
            ->all();

        return $courses;
    }

    public function courseTypesList()
    {
        $coursetypes = CourseType::find()
            ->all();

        return $coursetypes;
    }

    public function marketsList()
    {
        $markets = Market::find()
            ->all();

        return $markets;
    }

    public function marketsChecks()
    {
        $checks = EF::$plugin->efCoursesShared->getMarketsChecks();

        return $checks;
    }


    /*
     * Gets the status of the plugin to show on the status screen
     *
     * - If there are marketCodes set and if we have enough entries for the current sites.
     * - If we have locations
     * - If we have courses
     *
     */

    public function getPluginStatus()
    {
        $markets = $this->marketsList();
        if(count($markets) === 0){
            return [
                'msg' => "Please add some markets, and associate them to sites",
                'action' => [
                    'link' => UrlHelper::cpUrl('ef-courses/markets/add-market'),
                    'text' => 'Add Market'
                ]
            ];
        }

        $cities = (int) City::find()
            ->count();

        if ($cities < 1){
            return [
                'msg' => "Please do first fetch",
                'action' => [
                    'link' => UrlHelper::cpUrl('ef-courses/shared/first-fetch'),
                    'text' => 'Fetch Cities and Course Types'
                ]
            ];
        }

        return [
            'msg' => "You can update the prices by clicking the button below.",
            'action' => [
                'link' => UrlHelper::actionUrl('ef-courses/shared/update-prices-job'),
                'text' => 'Update Prices'
            ]
        ];

    }

    /*
     * Will get the market code, for the current site opened
     *
     * TODO: What should be done if the current site doesn't have a market code set?
     *
     */

    public function getMarketCode()
    {
        $site = Craft::$app->sites->getCurrentSite();
        $currentMarket = Market::find()
            ->where(['siteId' => $site->id])
            ->one();

        return $currentMarket->marketCode;
    }
}
