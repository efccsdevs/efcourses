<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\migrations;

use ef\efcourses\EfCourses;

use Craft;
use craft\config\DbConfig;
use craft\db\Migration;

/**
 * EF Courses Install Migration
 *
 * If your plugin needs to create any custom database tables when it gets installed,
 * create a migrations/ folder within your plugin folder, and save an Install.php file
 * within it using the following template:
 *
 * If you need to perform any additional actions on install/uninstall, override the
 * safeUp() and safeDown() methods.
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class Install extends Migration
{
    // Public Properties
    // =========================================================================

    /**
     * @var string The database driver to use
     */
    public $driver;

    // Public Methods
    // =========================================================================

    /**
     * This method contains the logic to be executed when applying this migration.
     * This method differs from [[up()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[up()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeUp()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        if ($this->createTables()) {
            // $this->createIndexes();
            // $this->addForeignKeys();
            // Refresh the db schema caches
            Craft::$app->db->schema->refresh();
            $this->insertDefaultData();
        }

        return true;
    }

    /**
     * This method contains the logic to be executed when removing this migration.
     * This method differs from [[down()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[down()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeDown()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        $this->removeTables();

        return true;
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates the tables needed for the Records used by the plugin
     *
     * @return bool
     */
    protected function createTables()
    {
        $tablesCreated = false;

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_productcodes}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_productcodes}}',
                [
                    'id' => $this->primaryKey(),
                    'productName' => $this->string(255)->notNull(),
                    'productCode' => $this->string(255)->notNull(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_productcodesdetails}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_productcodesdetails}}',
                [
                    'id' => $this->primaryKey(),
                    'productCodeId' => $this->integer()->notNull(),
                    'productCodeDetail' => $this->string(255)->notNull(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
            $this->addForeignKey($this->db->getForeignKeyName('{{%ef_productcodesdetails}}', 'productCodeId'), '{{%ef_productcodesdetails}}', ['productCodeId'], '{{%ef_productcodes}}', ['id'], 'CASCADE', null);
        }

    // efcourses_efcoursesrecord table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_countries}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_countries}}',
                [
                    'id' => $this->primaryKey(),
                    'countryName' => $this->string(255)->notNull(),
                    'countryValue' => $this->string(255)->notNull()->defaultValue(''),
                    'countryCode' => $this->string(255)->notNull()->defaultValue(''),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_cities}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_cities}}',
                [
                    'id' => $this->primaryKey(),
                    'countryId' => $this->integer()->notNull(),
                    'cityName' => $this->string(255)->notNull(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
            $this->addForeignKey($this->db->getForeignKeyName('{{%ef_cities}}', 'countryId'), '{{%ef_cities}}', ['countryId'], '{{%ef_countries}}', ['id'], 'CASCADE', null);
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_cityfields}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_cityfields}}',
                [
                    'id' => $this->primaryKey(),
                    'cityId' => $this->integer()->notNull(),
                    'productCodeId' => $this->integer()->notNull(),
                    'valueName' => $this->string(255)->notNull(),
                    'value' => $this->string(255)->notNull(),
                    'enable' => $this->boolean()->notNull()->defaultValue(true),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
            $this->addForeignKey($this->db->getForeignKeyName('{{%ef_cityfields}}', 'cityId'), '{{%ef_cityfields}}', ['cityId'], '{{%ef_cities}}', ['id'], 'CASCADE', null);
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_coursetypes}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_coursetypes}}',
                [
                    'id' => $this->primaryKey(),
                    'productCodeId' => $this->integer()->notNull(),
                    'courseType' => $this->string(255)->notNull()->defaultValue(''),
                    'courseTypeAlt' => $this->string(255)->notNull()->defaultValue(''),
                    'enable' => $this->boolean()->notNull()->defaultValue(false),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_coursetypefields}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_coursetypefields}}',
                [
                    'id' => $this->primaryKey(),
                    'courseTypeId' => $this->integer()->notNull(),
                    'valueName' => $this->string(255)->notNull()->defaultValue(''),
                    'value' => $this->text()->notNull(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_courses}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_courses}}',
                [
                    'id' => $this->primaryKey(),
                    'cityId' => $this->integer()->notNull(),
                    'courseTypeId' => $this->integer()->notNull(),
                    'courseNumber' => $this->integer(),
                    'startDate' => $this->dateTime(),
                    'minPrice' => $this->integer(),
                    'duration' => $this->integer(),
                    'courseContent' => $this->text()->notNull(),
                    'enable' => $this->boolean()->defaultValue(true),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%ef_markets}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%ef_markets}}',
                [
                    'id' => $this->primaryKey(),
                    'siteId' => $this->integer()->notNull(),
                    'marketCode' => $this->string(255)->notNull(),
                    'mainLocation' => $this->string(255)->notNull(),
                    'enable' => $this->boolean()->defaultValue(true),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                ]
            );
        }

        return $tablesCreated;
    }

    /**
     * Creates the indexes needed for the Records used by the plugin
     *
     * @return void
     */
    protected function createIndexes()
    {
    // efcourses_efcoursesrecord table
        $this->createIndex(
            $this->db->getIndexName(
                '{{%ef_countries}}',
                'countryName',
                true
            ),
            '{{%ef_countries}}',
            'countryName',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }
    }

    /**
     * Creates the foreign keys needed for the Records used by the plugin
     *
     * @return void
     */
    protected function addForeignKeys()
    {
    // efcourses_efcoursesrecord table
        $this->addForeignKey(
            $this->db->getForeignKeyName('{{%ef_countries}}', 'siteId'),
            '{{%ef_countries}}',
            'siteId',
            '{{%sites}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * Populates the DB with the default data.
     *
     * @return void
     */
    protected function insertDefaultData()
    {
    }

    /**
     * Removes the tables needed for the Records used by the plugin
     *
     * @return void
     */
    protected function removeTables()
    {
        // efcourses_efcoursesrecord table
        $this->dropTableIfExists('{{%ef_markets}}');
        $this->dropTableIfExists('{{%ef_courses}}');
        $this->dropTableIfExists('{{%ef_coursetypefields}}');
        $this->dropTableIfExists('{{%ef_coursetypes}}');
        $this->dropTableIfExists('{{%ef_productcodesdetails}}');
        $this->dropTableIfExists('{{%ef_productcodes}}');
        $this->dropTableIfExists('{{%ef_cityfields}}');
        $this->dropTableIfExists('{{%ef_cities}}');
        $this->dropTableIfExists('{{%ef_countries}}');
    }
}
