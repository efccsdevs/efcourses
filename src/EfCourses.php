<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses;

use ef\efcourses\services\EfCoursesLT as EfCoursesLT;
use ef\efcourses\services\EfCoursesILS as EfCoursesILS;
use ef\efcourses\services\EfCoursesShared as EfCoursesShared;
use ef\efcourses\services\Payments as Payments;

use ef\efcourses\variables\EfCoursesVariable;
use ef\efcourses\models\Settings;
use ef\efcourses\fields\Product as ProductField;
use ef\efcourses\fields\City as CityField;
use ef\efcourses\fields\Course as CourseField;
use ef\efcourses\utilities\EfCoursesUtility as EfCoursesUtilityUtility;
use ef\efcourses\controllers\LT as LTController;
use ef\efcourses\controllers\ILS as ILSController;
use ef\efcourses\controllers\Shared as SharedController;
use ef\efcourses\controllers\Payments as PaymentsController;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\services\Fields;
use craft\services\Utilities;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 *
 * @property  EfCoursesServiceService $efCoursesService
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class EfCourses extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * EfCourses::$plugin
     *
     * @var EfCourses
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '0.0.1';

    public $hasCpSection = false;


    public $controllerMap = [
        'lt' => LTController::class,
        'ils' => ILSController::class,
        'shared' => SharedController::class,
        'payments' => PaymentsController::class,
    ];

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * EfCourses::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'efCoursesLT' => EfCoursesLT::class,
            'efCoursesILS' => EfCoursesILS::class,
            'efCoursesShared' => EfCoursesShared::class,
            'efCoursesPayments' => Payments::class,
        ]);

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'ef-courses/lt';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['POST ef-courses/markets/add-market'] = 'ef-courses/shared/save-market';
                $event->rules['ef-courses/markets/edit-market/<id:\d+>'] = 'ef-courses/shared/add-market';
                $event->rules['ef-courses/markets/add-market'] = 'ef-courses/shared/add-market';
                $event->rules['POST ef-courses/locations'] = 'ef-courses/shared/save-associated';
            }
        );

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
            }
        );

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = ProductField::class;
                $event->types[] = CityField::class;
                $event->types[] = CourseField::class;
            }
        );

        // Register our utilities
        Event::on(
            Utilities::class,
            Utilities::EVENT_REGISTER_UTILITY_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = EfCoursesUtilityUtility::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('efCourses', EfCoursesVariable::class);
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        /**
         * Logging in Craft involves using one of the following methods:
         *
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
         *
         * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
         * the category to the method (prefixed with the fully qualified class name) where the constant appears.
         *
         * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
         * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
         *
         * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
         */
        Craft::info(
            Craft::t(
                'ef-courses',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );

        $fileTarget = new \craft\log\FileTarget([
            'logFile' => Craft::getAlias('@storage/logs/ef-courses.log'),
            'categories' => ['ef-courses']
        ]);
        // include the new target file target to the dispatcher
        Craft::getLogger()->dispatcher->targets[] = $fileTarget;

    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {
        $shainSet = false;
        if (getenv("OGONE_SHAIN")) {
            $shainSet = true;
        }

        return Craft::$app->view->renderTemplate(
            'ef-courses/settings',
            [
                'settings' => $this->getSettings(),
                'shainSet' => $shainSet
            ]
        );
    }

    public static function log($message)
    {
        Craft::getLogger()->log($message, \yii\log\Logger::LEVEL_INFO, 'ef-courses');
    }
}

