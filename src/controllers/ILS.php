<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\controllers;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\web\Controller;
use ef\efcourses\jobs\EfCoursesILSTask;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class ILS extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'fetch-locations','do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/ef-courses/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

//        var_dump(EF::$plugin->efCoursesLT->fetchLocations());
//        var_dump(EF::$plugin->efCoursesLT->fetchCourses());

        //EF::$plugin->EfCoursesService->fetchLocations();
        exit();
        return $result;
    }

    public function actionFetchLocations()
    {
        $result = EF::$plugin->efCoursesILS->fetchLocations();

        return $this->asJson($result);
    }

    public function actionFetchCourses()
    {
        $result = EF::$plugin->efCoursesILS->fetchCourses();

        return $this->asJson($result);
    }

    public function actionToggleCourseType()
    {
        $id = Craft::$app->request->post('elementid');
        $value = Craft::$app->request->post('value');

        $result = EF::$plugin->efCoursesLT->toggleCourseType($id, $value);

        return $this->asJson($result);
    }

    public function actionLocationsCourses()
    {
        $uses = Craft::$app->request->post('uses');

        $result = EF::$plugin->efCoursesILS->updateLocationCourses($uses);

        return $this->asJson($result);
    }

    public function actionUpdatePricesJob()
    {
        Craft::$app->queue->push(new EfCoursesILSTask([
            'description' => 'Update ILS Courses',
        ]));
        return $this->redirect('ef-courses');
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/ef-courses/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }
}
