<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\controllers;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\web\Controller;
use ef\efcourses\records\Market;
use ef\efcourses\jobs\EfCoursesLTTask;
use ef\efcourses\jobs\EfCoursesILSTask;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class Shared extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'fetch-locations','do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/ef-courses/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

//        var_dump(EF::$plugin->efCoursesLT->fetchLocations());
//        var_dump(EF::$plugin->efCoursesLT->fetchCourses());

        //EF::$plugin->EfCoursesService->fetchLocations();
        exit();
        return $result;
    }

    public function actionUpdateCoursesQueue()
    {
        $pids = EF::$plugin->efCoursesLT->getProductCodeIds();

        $cids = EF::$plugin->efCoursesLT->getCitiesIds();

        Craft::$app->getQueue()->push(new EfCoursesTask([
            'pids' => $pids,
            'cids' => cids,
        ]));
    }

    public function actionUpdatePricesJob()
    {
        Craft::$app->queue->push(new EfCoursesLTTask([
            'description' => 'Update LT Courses',
        ]));

        Craft::$app->queue->push(new EfCoursesILSTask([
            'description' => 'Update ILS Courses',
        ]));

        return $this->redirect('ef-courses');
    }

    public function actionFirstFetch()
    {
        Craft::$app->queue->push(new EFCoursesFirstFetchTask([
            'description' => 'First Fetch',
        ]));
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/ef-courses/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }

    public function actionSaveAssociated()
    {
        $craft = \Craft::$app;

        // Fetch data from post (format ['city_2' = 23])
        $locations = [];
        foreach(Craft::$app->request->post() as $key => $info){
            if(strpos($key, "city_") === 0){
                $locations[explode('_', $key)[1]] = $info;
            }
        }

        $result = EF::$plugin->efCoursesShared->updateDestinationAssociations($locations);


        $craft->session->setNotice(
            \Craft::t('ef-courses', 'Locations updated')
        );

        $this->redirectToPostedUrl();

    }

    public function actionAddMarket(int $id = -1)
    {
        // Get all sites to pass to form

        $sites = [];

        foreach(Craft::$app->sites->getAllSites() as $site){
            $sites[$site->id] = $site->name;
        }

        $market = [
            'siteId' => '',
            'marketCode' => '',
            'mainLocation' => '',
            'empty' => true,
        ];

        if($id > 0){
            $market = Market::find()
                ->where(['id' => $id])
                ->asArray()
                ->one();
        }

        // redirect to form
        return $this->renderTemplate('ef-courses/markets/add-market', [
            'sites' => $sites,
            'market' => $market,
            'edit' => $id > 0 ? true : false,
        ]);
    }

    public function actionSaveMarket(string $id = null)
    {
        $craft = \Craft::$app;

        // Fetch data from post (format ['city_2' = 23])

        $marketObj = [
            'siteId' =>  $craft->request->post('siteId'),
            'marketCode' => $craft->request->post('marketCode'),
            'mainLocation' => $craft->request->post('mainLocation'),
        ];


        $result = EF::$plugin->efCoursesShared->saveMarket($marketObj);


        $craft->session->setNotice(
            \Craft::t('ef-courses', 'Market Added')
        );

        $this->redirectToPostedUrl();

    }
}
