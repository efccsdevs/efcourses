<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\controllers;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\web\Controller;

/**
 * Payments Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class Payments extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'fetch-payment-url', 'process-payment'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/ef-courses/payments
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the PaymentsController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/ef-courses/payments/do-something
     *
     * @return mixed
     */
    public function actionFetchPaymentUrl()
    {

        if( Craft::$app->request->get('webFormSubmissionID') == false ||
            Craft::$app->request->get('reserveRoot') == false ||
            Craft::$app->request->get('brand') == false)
        {
            // TODO check if at least one of firstPayment or enrollmentFee is not zero
            return $this->asJson(['paymentUrl' => '', 'error' => 'parametersMissing']);
        }

        $requestToEF = array(
            'webFormSubmissionID' => Craft::$app->request->get('webFormSubmissionID'), //25603233
            'paymentType' => 'EF',
            'currencyCode' => 'EUR',
            'enrollmentFee' => Craft::$app->request->get('enrollmentFee'), // From CMS?
            'firstPayment' => 0, // From CMS?
            'reserveRoot' => Craft::$app->request->get('reserveRoot'), // (lt or ils)
            'mc' => Craft::$app->request->get('mc')? Craft::$app->request->get('mc') : 'FI',
            'brand' => Craft::$app->request->get('brand'),
            'nolang' => 1,
        );

        $result = EF::$plugin->efCoursesPayments->fetchPaymentUrl($requestToEF);

        return $this->asJson($result);
    }

    public function actionProcessPayment()
    {
        $result = EF::$plugin->efCoursesPayments->processPayment();

        return $this->asJson($result);

    }
}
