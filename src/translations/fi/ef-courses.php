<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

/**
 * EF Courses en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('ef-courses', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
return [
    'EF Courses plugin loaded' => 'EF Courses plugin loaded',
    '{value} €'
];
