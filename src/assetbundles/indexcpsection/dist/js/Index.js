/**
 * EF Courses plugin for Craft CMS
 *
 * Index Field JS
 *
 * @author    EF Global Creative
 * @copyright Copyright (c) 2019 EF Global Creative
 * @link      https://ef.design
 * @package   EfCourses
 * @since     0.0.1
 */

var fetching = false;
var windowChanged = false;

$(document).ready(function() {

    $('[id^="toUse_"]').on('click', function(event){
        event.stopPropagation();
        if ($(event.currentTarget).hasClass('disabled')) {
            return false;
        }

        $elm = $(this).find('[name^="use_"]');
        elmid = $elm.attr('name').split('_')[1];
        elmval = $elm.val();

        elmbol = (elmval === '') ? false : true;

        var data = {
            'CRAFT_CSRF_TOKEN': $('[name="CRAFT_CSRF_TOKEN"]').val(),
            'elementid': elmid,
            'value': elmbol,
        };

        // Disable button
        $(event.currentTarget).addClass('disabled');
        $(event.currentTarget).find('[name^="use_"]').attr('disabled', true);

        $.post({
            url: $('input[name="switchUrl"]').val(),
            data: data,
            success: function(data) {
                $(event.currentTarget).removeClass('disabled');
                $(event.currentTarget).find('[name^="use_"]').attr('disabled', false);
                $(event.currentTarget).after('<span class="success" title="Passed" data-icon="check"></span>');
                setTimeout(function(){ $(event.currentTarget).next('span.success').fadeOut('250',function(){ this.remove();})}, 1000);
            }
        })
    });

    $('#fetchResourseButton').on('click', function(event) {
        event.preventDefault();
        if ($(event.currentTarget).hasClass('disabled')){
            return false;
        }

        $(event.currentTarget).addClass('disabled');
        $('#efCoursesStatus').append('<p id="EFloading">Processing, please wait...</p>');
        fetching = true;
        var jqxhr = $.get({
            url: $(this).attr('href'),
            success: function(data) {
                fetching = false;
                $('#EFloading').remove();
                if (data !== '') {
                    $('#efCoursesStatus').html('<span class="success" title="Passed" data-icon="check"></span> ' + JSON.stringify(data));
                }else{
                    $('#efCoursesStatus').html('<span class="success" title="Passed" data-icon="check"></span> ' + 'No changes done');
                }
                $(event.currentTarget).removeClass('disabled');
            },
        });

        jqxhr.fail(function(data) {
            fetching = false;
            $('#EFloading').remove();
            $('#efCoursesStatus').html('<span class="error" title="Passed" data-icon="alert"></span> There was an error during the request!');
            $(event.currentTarget).removeClass('disabled');
        })

        return false;
    });

    $('form#main-form').on('submit', function(ev){
        if ($('[name="action"]').val() === 'location-courses') {
            fetching = true;

            const uses = [];
            $('[name^="use_"]').each(function(idx, elem) {
                elm = $(elem).attr('name').split('_');
                uses.push({
                    'id': elm[1],
                    'value': ($(elem).val() === '') ? 0 : 1,
                });
            });

            var jqxhr = $.post($('[name="post-url"]').val(), {
                    'CRAFT_CSRF_TOKEN': $('[name="CRAFT_CSRF_TOKEN"]').val(),
                    uses
                },
                function(data) {
                    fetching = false;
                    $('#EFloading').remove();
                    if (data !== '') {
                        $('#efCoursesStatus').html('<span class="success" title="Passed" data-icon="check"></span> ' + JSON.stringify(data));
                    }else{
                        $('#efCoursesStatus').html('<span class="success" title="Passed" data-icon="check"></span> ' + 'No changes done');
                    }
                    $(event.currentTarget).removeClass('disabled');
                },
            )
                .fail(function(data){
                    fetching = false;
            });
            return false;
        }
    });

    $(window).bind('beforeunload', function() {
        if (fetching) {
            return confirm('Data is still being fetched! Do you really want to close?');
        }

        if (windowChanged) {
            return confirm('Data has been changed! Do you really want to close?');
        }
    });
});