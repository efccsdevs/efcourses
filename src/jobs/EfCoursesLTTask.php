<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\jobs;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\queue\BaseJob;
use ef\efcourses\records\ProductCode;

/**
 * EfCoursesTask job
 *
 * Jobs are run in separate process via a Queue of pending jobs. This allows
 * you to spin lengthy processing off into a separate PHP process that does not
 * block the main process.
 *
 * You can use it like this:
 *
 * use ef\efcourses\jobs\EfCoursesTask as EfCoursesTaskJob;
 *
 * $queue = Craft::$app->getQueue();
 * $jobId = $queue->push(new EfCoursesTaskJob([
 *     'description' => Craft::t('ef-courses', 'This overrides the default description'),
 *     'someAttribute' => 'someValue',
 * ]));
 *
 * The key/value pairs that you pass in to the job will set the public properties
 * for that object. Thus whatever you set 'someAttribute' to will cause the
 * public property $someAttribute to be set in the job.
 *
 * Passing in 'description' is optional, and only if you want to override the default
 * description.
 *
 * More info: https://github.com/yiisoft/yii2-queue
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class EfCoursesLTTask extends BaseJob
{
    // Public Properties
    // =========================================================================

    /**
     * Some attribute
     *
     * @var string
     */
    public $someAttribute = 'Some Default';

    public $pids;

    public $cids;

    // Public Methods
    // =========================================================================

    /**
     * When the Queue is ready to run your job, it will call this method.
     * You don't need any steps or any other special logic handling, just do the
     * jobs that needs to be done here.
     *
     * More info: https://github.com/yiisoft/yii2-queue
     */
    public function execute($queue)
    {
        // This will update the LT courses
//        $pcs = ProductCode::findAll(['id' => $this->pids]);
//        $pc = null;
//        if(count($pcs) < 2){
//            $pc = $pcs[0];
//        }

        // Get number of cities
        $cities = EF::$plugin->efCoursesLT->fetchCities();

        $totalSteps = count($cities);
        $step = 1;

        // for each city, fetch courses and do the thing
        foreach($cities as $city){
            $this->setProgress($queue, $step / $totalSteps);

            $result = EF::$plugin->efCoursesLT->fetchCity($city);

            $logger = Craft::getLogger();

            $cityLog = ['city' => $city->value];
            $logger->log(json_encode($cityLog).json_encode($result), $logger::LEVEL_INFO, 'EF Course Task');

            $step++;
        }

        // Do work here
    }

    // Protected Methods
    // =========================================================================

    /**
     * Returns a default description for [[getDescription()]], if [[description]] isn’t set.
     *
     * @return string The default task description
     */
    protected function defaultDescription(): string
    {
        return Craft::t('ef-courses', 'Updating ILS Courses');
    }
}
