<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\services;

use ef\efcourses\EfCourses;

use Craft;
use craft\base\Component;

use craft\elements\Entry;
use GuzzleHttp\Client;
use ef\efcourses\records\Country;
use ef\efcourses\records\City;
use ef\efcourses\records\CityField;
use ef\efcourses\records\ProductCode;
use ef\efcourses\records\ProductCodeDetail;
use ef\efcourses\records\CourseType;
use ef\efcourses\records\CourseTypeField;
use ef\efcourses\records\Course;

/**
 * EfCoursesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class EfCoursesLT extends Component
{
    public $productCodes = [
        [
            'productName' => 'LT',
            'productCode' => 'lt',
            'productCodeDetail' => ['13','14']
        ],
    ];

    private $domain = "https://qa.ef.fi";
    private $marketCode = "FI";
    private $productObj = [];
    private $currentProduct = null;
    private $status = [];
    private $validLocations = ['Madrid', 'Helsinki'];

    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     EfCourses::$plugin->efCoursesService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';
        // Check our Plugin's settings for `someAttribute`
        if (EfCourses::$plugin->getSettings()->someAttribute) {
        }

        return $result;
    }

    public function init()
    {
        parent::init();

        $this->productObj = $this->checkProductCode();

        if(count($this->productObj) < 2){
            $this->currentProduct = $this->productObj[0];
        }

    }

    private function checkProductCode()
    {
        $PCEntries = [];

        // Check if all product codes are saved
        foreach($this->productCodes as $productCode){
            $productObj = ProductCode::find()
                ->where(['productName' => $productCode['productName']])
                ->andWhere(['productCode' => $productCode['productCode']])
                ->with('details')
                ->one();

            if ($productObj === null) {
                $productObj = new ProductCode();
                $productObj->productName = $productCode['productName'];
                $productObj->productCode = $productCode['productCode'];
                $productObj->save();
            }

            // Check for the extras
            foreach ($productCode['productCodeDetail'] as $pExtra) {
                $extraObj = ProductCodeDetail::find()
                    ->where(['productCodeId' => $productObj->id])
                    ->andWhere(['productCodeDetail' => $pExtra])
                    ->one();

                if ($extraObj === null) {
                    $extraObj = new ProductCodeDetail();
                    $extraObj->productCodeDetail = $pExtra;
                    $extraObj->link('productCode', $productObj);
                }
            }
            $PCEntries[] = $productObj;
        }

        return $PCEntries;
    }

    private function fetchRequest($reqUri)
    {
        $client = new \GuzzleHttp\Client();

        try{
            // $request = $client->createRequest('GET', $reqUri,[]);
            $response = $client->request('GET', $reqUri,[]);

            // $response = $request->send();

        }catch (Exception $e){
            return false;
        }

        return json_decode($response->getBody());
    }

    public function fetchLocations()
    {
        foreach ($this->productObj as $product) {
            $this->currentProduct = $product;
            foreach ($product->details as $detail) {
                $age = $detail->productCodeDetail;
                $destinations = $this->fetchRequest($this->domain.'/ef-services/reservationservice/booking/getltcreativedestinations/?programcode=LT&productCode=LT&marketcode=FI&agegroup=' . $age . '&mc=FI');

                foreach($destinations as $dest) {
                    $country = $this->saveCountry($dest);

                    $city = $this->saveCity($dest, $country);
                }
            }
        }

        return $this->status;
    }

    public function saveCountry($dest)
    {
        // Check if the Country exists, if not, save it.
        $country = Country::find()
            ->where(['CountryName' => $dest->CountryName])
            ->one();
        if ($country === null) {
            $country = new Country();
            $country->countryName = $dest->CountryName;
            $country->save();

            $this->addStatus('newCountry');
        }

        return $country;
    }

    public function saveCity($dest, $country)
    {
        // Check if the city exists, and if not, save it (both name and codes)
        $city = City::find()
            ->where(['cityName' => $dest->CityName])
            ->andWhere(['countryId' => $country->id])
            ->one();

        if ($city === null) {
            $city = new City();
            $city->countryId = $country->id;
            $city->cityName = $dest->CityName;
            $city->save();
            $this->addStatus('newCity');
            
        }

        // Lets save the extra City Fields (Destination Code, etc)
        $cityField = CityField::find()
            ->where(['cityId' => $city->id])
            ->andWhere(['productCodeId' => $this->currentProduct->id])
            ->andWhere(['value' => $dest->DestinationsCodesWithAccom])
            ->one();

        if ($cityField === null) {
            // Check if we have any fields with DestinationsCodesWithAccom, that we need to disable
//          Commenting this code for now, until we find a situation that requires it
//            $cityField = CityField::find()
//                ->where(['valueName' => 'DestinationsCodesWithAccom'])
//                ->andWhere(['value' => $dest->DestinationsCodesWithAccom])
//                ->andWhere(['enable' => 1])
//                ->one();
//
//            if ($cityField !== null) {
//                $cityField->enable = 0;
//                $cityField->save();
//            }

            // Now lets save the new value
            $cityFieldNew = new CityField();
            $cityFieldNew->cityId = $city->id;
            $cityFieldNew->valueName = 'DestinationsCodesWithAccom';
            $cityFieldNew->value = $dest->DestinationsCodesWithAccom;
            $cityFieldNew->enable = true;
            $cityFieldNew->link('product', $this->currentProduct);

            // TODO: Check if there are multiple fields for this city (shouldn't)
        }


        return $city;
    }

    public function fetchCourses()
    {
        foreach ($this->productObj as $product){
            $this->currentProduct = $product;

            // Get all cities
            $cities = CityField::find()
                ->where(['productCodeId' => $product->id])
                ->with('city')
                ->all();

            foreach ($cities as $city){
                $course = $this->fetchCity($city);
            }
        }

        return $this->status;
    }

    public function fetchCity($city)
    {
        $courseTypes = CourseType::find()
            ->select('courseType')
            ->where(['productCodeId' => $this->currentProduct->id])
            ->andWhere(['enable' => true])
            ->asArray()
            ->all();

        $LTCourses = $this->fetchRequest($this->domain.'/ef-services/reservationservice/booking/getltcreativeprices/?programcode=LT&productcode=LT&marketcode=FI&destinationcodes=' . $city->value . '&mc=FI');

        // Filter out courses that DON'T departure from the main city, as those are more expensive
        // And filter out course types  that were not selected
        $filteredCourses = array_filter($LTCourses, function($course) use ($courseTypes) {
            if(in_array(trim($course->DeparturePoint), $this->validLocations))
                //&& in_array($course->CourseTypeAbbr, $courseTypes))
                return true;
        });

        foreach ($filteredCourses as $course)
        {
            // Get/Save CourseType
            $courseType = $this->saveCourseType($course);

            // If the course is not enabled let's skip the course saving
            if (!$courseType->enable) {
                continue;
            }

            // Get/Save Course

            $course = $this->saveCourse($course, $courseType, $city);
        }

    }

    public function saveCourseType($course)
    {
        // Check if the courseType exists
        $courseType = CourseType::find()
            ->where(['courseType' => $course->CourseTypeAbbr])
            ->one();

        if ($courseType === null) {
            $courseType = new CourseType();
            $courseType->courseType = $course->CourseTypeAbbr;
            $courseType->link('product', $this->currentProduct);
            $this->addStatus('newCourseType');

            // Save all fields
            $fields = ['CourseType'];

            foreach ($fields as $field) {
                $courseTypeField = new CourseTypeField();
                $courseTypeField->valueName = $field;
                $courseTypeField->value = trim($course->{$field});

                $courseTypeField->link('courseType', $courseType);
            }
        }

        return $courseType;
    }

    public function saveCourse($course, $courseType, $city)
    {
        $courseObj = Course::find()
            ->where(['courseNumber' => $course->CourseNumber])
            ->one();

        if ($courseObj === null) {
            $course->compulsoryExtras = $this->getCompulsoryExtras($course->DestinationCode, $course->StartDate, $course->EndDate, $course->CourseKey);

            $courseObj = new Course;
            $courseObj->cityId = $city->id;
            $courseObj->courseNumber = $course->CourseNumber;
            $courseObj->startDate = $course->StartDate;
            $courseObj->minPrice = $course->BasePrice;
            $courseObj->duration = $course->NoOfDays;
            $courseObj->courseContent = json_encode($course);
            $courseObj->link('courseType', $courseType);
            $this->addStatus('newCourse');
        } else {
            // Check if there are differences and update course
            if($courseObj->minPrice != $course->BasePrice) {
                //&& $courseObj->startDate === $courseStartDate
                $courseObj->minPrice = $course->BasePrice;
                $courseObj->courseContent = json_encode($course);
                $courseObj->save();
                $this->addStatus('updateCourse');
            } else {
                $this->addStatus('skipCourse');
            }
        }
        //TODO: finish add info to course, find out best option to update info if needed.

        return $courseObj;
    }

    private function getCompulsoryExtras($city, $startDate, $endDate, $courseKey)
    {
        // We need to make a request for this specific trip, so we can have the compulsory extras saved for later

        $cityValue = explode('|',$city)[0];

        $courseDetailsParameters = [
            'programcode=LT',
            'productcode=LT',
            'marketcode=' . $this->marketCode,
            'destinationcode=' . $cityValue,
            'depaturepoint=Helsinki',
            'selecteddate=' . $this->getSelecteddate($startDate, $endDate),
            'coursekey=' . $courseKey,
            'nolang=1',
            'mc=' . $this->marketCode
        ];

        $courseDetails = $this->fetchRequest($this->domain . '/ef-services/reservationservice/booking/GetLTExtras/?'.implode('&',$courseDetailsParameters));

        // Find all extras that are Compulsory
        if(!is_array($courseDetails->BookingExtras)){
            return [];
        }
        $compulsoryExtras = [];
        foreach($courseDetails->BookingExtras as $extra){
            if($extra->IsCompulsory){
                $compulsoryExtras[$extra->Articlecode] = $extra->Price;
            }
        }

        return $compulsoryExtras;
    }

    private function getSelecteddate($startdate, $enddate)
    {
        $start = date('c', strtotime($startdate));
        $end = date('c', strtotime($enddate));

        return $start . '-'. $end;
    }

    private function addStatus($status)
    {
        if (!isset($this->status[$status])) {
            $this->status[$status] = 0;
        }

        $this->status[$status]++;
    }

    public function toggleCourseType($id, $value)
    {
        $courseType = CourseType::find()
            ->where(['id' => $id])
            ->one();

        if ($courseType === null) {
           return false;
        }

        $value = ($value === 'false') ? false : true;

        $courseType->enable = $value;
        if ($courseType->save()) {
            return true;
        }

        return false;
    }

    public function fetchCities()
    {
        $cities = CityField::find()
            ->where(['productCodeId' => $this->currentProduct->id])
            ->with('city')
            ->all();

        return $cities;
    }

    // TODO: Check if this is working correctly when MyTrips is
    public function getPricesForCourseTypes($courseTypes, $entry)
    {
        $cityID = unserialize($entry->destinations->level(3)->one()->efCity);
        $courseTypes = explode("|", $courseTypes);

        // Select from citiescourses where cityID = $cityID and coursetypeID in $idList of courseType in $courseTypesArr

        $prices = Course::find()
            ->select('ef_courses.id, ct.courseType, cityId, minPrice, courseContent')
            ->joinWith(['courseType ct' => function($query) {
               $query->select('ct.id, courseType as courseType');
            }])
            ->where('cityId = :cityId', [':cityId' => $cityID[0]])
            ->andWhere(['IN', 'courseType', $courseTypes])
            ->createCommand()->queryAll();

        foreach($prices as &$price){
            $price['courseContent'] = json_decode($price['courseContent'], 'true');
        }

        return $prices;
    }

    // TODO: check for MyTrips and more
    public function getCoursesFrontend($courseTypes, $associative = false, $assocKey = 'courseType')
    {
        $productCodes = $this->checkProductCode();
        $pcArray = [];
        foreach($productCodes as $productCode)
        {
            $pcArray[] = $productCode->id;
        }

        $courseTypesArr = explode('|', $courseTypes);

        // TODO: fetch only the products of the correct age
        $courses = CourseType::find()
            ->where(['IN', 'courseType', $courseTypesArr])
            ->andWhere(['IN', 'productCodeId', $pcArray])
            ->andWhere('enable = 1')
            ->with('fields')
            ->all();

        $coursesArr = [];
        foreach ($courses as $course){
            $coursesArr[] = $course->id;
        }


        $coursesEntry = Entry::find()
            ->section('pages')
            ->where(['or',['IN', 'field_efCourse', $coursesArr]])
            ->all();

        $coursesLT = [];

        foreach($coursesEntry as $course)
        {
            $tC['CourseNameLabel'] = $course['courseName']?$course['courseName']:$course['title'];
            $tC['CourseDescriptionLabel'] = $course['courseDescription']?$course['courseDescription']:$course['tagLine'];
            $courseType = CourseType::find()->where(['id' => $course['efCourse']])->one();
            $tC['courseType'] = $courseType->courseType;
            $tC['hasRibbon'] = $course['isPopular'] ? true : false;
            $tC['ribbonLabel'] = Craft::t('site', 'Popular');

            if ($associative) {
                $coursesLT[$course[$assocKey]] = $tC;
            }else{
                $coursesLT[] = $tC;
            }
        }

        if(empty($coursesEntry)){
            // No Courses pages, lets at least make the booking work!
            $courses = CourseType::find()
                ->with('fields')
                ->where(['IN', 'courseType', $courseTypesArr])
                ->all();

            foreach($courses as $course){
                $tC['CourseNameLabel'] = $course->fields[0]->value;
                $tC['CourseDescriptionLabel'] = "";
                $tC['courseType'] = $course['courseType'];
                $tC['hasRibbon'] = false;
                $tC['ribbonLabel'] = Craft::t('site', 'Popular');

                if ($associative) {
                    $coursesLT[$course[$assocKey]] = $tC;
                }else{
                    $coursesLT[] = $tC;
                }
            }
        }

        return $coursesLT;
    }
}
