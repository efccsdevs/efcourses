<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\services;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\base\Component;
use craft\helpers\UrlHelper as UrlHelper;

use GuzzleHttp\Client;

/**
 * Payments Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class Payments extends Component
{

    private $settings;

    public function init()
    {
//        $this->settings = craft()->plugins->getPlugin('EfPayments')->getSettings();
    }

    private function implosionKeys($array, $separator)
    {

        $output = implode($separator, array_map(
            function ($v, $k) { return sprintf("%s=%s", $k, $v); },
            $array,
            array_keys($array)
        ));

        return $output;
    }

    private function explosionKeys($array, $separator){
        for ($i=0; $i < sizeof($array); $i++)
        {
            $tempExploded = explode($separator, $array[$i]);

            if(count($tempExploded) <= 2){
                list($key,$value) = $tempExploded;
            }else{
                $key = $tempExploded[0];
                $v = array_shift($tempExploded);
                $value = implode($separator, $tempExploded);
            }
            $output[$key] = $value;
        }
        return $output;
    }

    // Public Methods
    // =========================================================================


    /**
     * This function connects to the EF API to receive the url that should be used to show the credit card payment form
     *
     * Because we're using a function from EF, it already brings the return urls (ACCEPTURL and EXCEPTIONURL) pointing
     * to the EF websites. Because of this, we need to edit those fields, so we deconstruct the params, update with
     * what we need, and sign the request again, before sending it to Ogone. If we didn't sign it, the request would
     * failed, since it was tampered with.
     *
     * @return mixed
     */
    public function fetchPaymentUrl($requestParams)
    {

        if(!isset(Craft::$app->config->general->EFApiUrl)){
            Craft::error('EFApiUrl config item is not set, please set it in config/general.php for this environment', 'efcourses');
            return ['paymentUrl' => '', 'error' => 'configParametersMissing'];
        }

        // Send request to GetOgoneSource to received the ALIAS.ORDERID
        $client = new \GuzzleHttp\Client();

        $efAPI = Craft::$app->config->general->EFApiUrl;
        $reqUri = $efAPI.'/ef-services/ReservationService/Ogone/GetOgoneSource/?'.$this->implosionKeys($requestParams, '&');

        try{
            $response = $client->request('POST', $reqUri, []);

//            $response = $request->send();

        }catch (Exception $e){
            return false;
        }

        $EFOgoneURL = (string) $response->getBody();

        if(strcmp($EFOgoneURL,"null") === 0){
            // TODO: agree on an error to return
            EF::log("Payment URL was not valid.");
            return
                [
                    "error" => true,
                    'return' => $EFOgoneURL,
                    'url' => $reqUri
                ];
        }
        $EFOgoneParams = $this->explosionKeys(explode('&', parse_url($EFOgoneURL,PHP_URL_QUERY)), '=');

        // Add ENTRYID to PARAMETERS.PARAMPLUS
        $paramsArray = $this->explosionKeys(explode('|', $EFOgoneParams['PARAMETERS.PARAMPLUS']), '=');
        $paramsArray['ENTRYID'] = Craft::$app->request->get('entryId');
        $paramsArray['AGEGROUP'] = Craft::$app->request->get('ageGroup');
        if (Craft::$app->request->get('userPromoId')) {
            $paramsArray['USERPROMOID'] = Craft::$app->request->get('userPromoId');
        }
        $EFOgoneParams['PARAMETERS.PARAMPLUS'] = $this->implosionKeys($paramsArray,'|');

        // Update ACCEPTURL and EXCEPTIONURL with our values
        $EFOgoneParams['PARAMETERS.ACCEPTURL'] = UrlHelper::siteUrl().'actions/ef-courses/payments/process-payment/';
        $EFOgoneParams['PARAMETERS.EXCEPTIONURL'] = UrlHelper::siteUrl().'actions/ef-courses/payments/process-payment/';
        $EFOgoneParams['LAYOUT.LANGUAGE'] = Craft::$app->locale->id;

        // Unset EF SHASIGN
        unset($EFOgoneParams['SHASIGNATURE.SHASIGN']);

        // Create our SHASIGN based on the new content
        $EFOgoneParams['SHASIGNATURE.SHASIGN'] = $this->generateSHA1($EFOgoneParams);

        // Create full Ogone URL to send to the client
        $paymentUrl = $this->implosionKeys($EFOgoneParams, '&');

        header('Access-Control-Allow-Origin: *');

        return ["paymentUrl" => Craft::$app->config->general->ogone.'?'.$paymentUrl, 'brand' => $EFOgoneParams['CARD.BRAND']];
    }

    /**
     *
     * Generates SHA1 to sign the request, this is needed because we edited the request that was already signed from
     * EF on the previous function that called this one.
     *
     * @return mixed
     */
    private function generateSHA1($data)
    {
        if(getenv("OGONE_SHAIN"))
            $password = getenv("OGONE_SHAIN");
        else
            $password = EF::getInstance()->settings->shainpassword;
        $hashstr = '';
        foreach($data as $key => $val){
            $hashstr .= $key.'='.$val.$password;
        }

        return sha1($hashstr);
    }


    /**
     *
     * Receives the request from the Ogone payment, and if everything is ok, connects to the EF API
     * to checks the status of the operation.
     *
     */
    public function processPayment(){

        $test = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $returndata = $this->explosionKeys(explode('&', parse_url($test,PHP_URL_QUERY)), '=');

        $returndata['PARAMPLUS'] = 'PAYMENTTYPE='.$returndata['PAYMENTTYPE'];
        $shaTemp = $returndata['SHASign'];
        unset($returndata['PAYMENTTYPE']);
        unset($returndata['SHASign']);
        $returndata['SHASign'] = $shaTemp;

        $OgoneQuery = $this->implosionKeys($returndata, '&');

        $paramsPlus = $this->explosionKeys(explode('|', urldecode($returndata['PARAMPLUS'])), '=');

        switch($returndata['Alias.Status']){
            case '0': // OK
                // Send request to DirectLink
                // POST https://qa.ef.com/ReservationService/Ogone/DirectLink
                //
                // {"OgoneQueryString":"mc=FI&Alias.AliasId=9005226B-7B1F-4E5D-A2EF-C1EC15F87B29&Card.Brand=VISA&Card.CardNumber=XXXXXXXXXXXX1111&Card.CardHolderName=blah&Card.ExpiryDate=1230&Alias.NCError=0&Alias.NCErrorCardNo=0&Alias.NCErrorCN=0&Alias.NCErrorED=0&Alias.OrderId=1015920&Alias.Status=0&PARAMPLUS=PAYMENTTYPE=EF%7cENROLLMENTFEE%3d397%7cFIRSTPAYMENT%3d0%7cPAYMENTPRICE%3d39700%7cCURRENCYCODE%3dEUR%7cWEBFORMSUBMISSIONID%3d25603233%7cMARKETCODE%3dFI%7cPROGRAMCODE%3dLT%7cFIRSTNAME%3dTest%7cLASTNAME%3dTest%7cMOBILE%3d123456123%7cEMAIL%3dblah%40blah.com%7cRESERVEROOT%3dlt%7cHOMEPHONE%3d&SHASign=DEA29A98E50DDD9E52B6A72E7DBD5116F360BBD4"}
                // OgoneQueryString = craft()->request->getQueryStringWithoutPath();

                // Send request to GetOgoneSource to received the ALIAS.ORDERID
                $uri = Craft::$app->config->general->EFApiUrl.'/ef-services/ReservationService/Ogone/DirectLink';
                $client = new \GuzzleHttp\Client();

                // Appending the marketcode to the url so it doesn't return the general language selection page.
                if(isset($paramsPlus['MARKETCODE'])) {
                    $uri.= '?mc='.$paramsPlus['MARKETCODE'];
                }

                $post_data = array(
                    'OgoneQueryString' => $OgoneQuery
                );

                $data = json_encode($post_data);

                try{
                    $response = $client->post($uri,
                        [
                            'headers' => ['content-type' => 'application/json'],
                            'body' => $data
                        ]);

                    $directLinkReply = (string) $response->getBody();

                    $errorMsg = "";

                    switch($directLinkReply){
                        case 1: // canceled by User
                            $errorMsg = "User canceled payment.";
                            break;
                        case 2: // Failure on FlexCheckout (No call on second step)
                            $errorMsg = "Failure on FlexCheckout";
                            break;
                        case 3: // Success on FlexCheckout and failure on DirectLink
                            $errorMsg = "Failure on DirectLink";
                            break;
                        case 4: // Sha Sign failure
                            $errorMsg = "SHA SIGN Failure";
                            break;
                        default:
                            $n = "entry=".$paramsPlus['ENTRYID']."&ord=".$returndata['Alias.OrderId'];
                            if(isset($paramsPlus['USERPROMOID'])){
                                $n .= "&userPromoId=".$paramsPlus['USERPROMOID'];
                            }
                            $ageGroup = $paramsPlus['AGEGROUP'] ? $paramsPlus['AGEGROUP']."/": "";
                            EF::log('A problem occurred, we\'ve redirected the user to the success page, but the payment didn\' go through. '.$errorMsg);
                            ?>
                            <html>
                            <body class="--theme-bg--default">
                            A problem occurred during the payment, you will be contacted by a member of our staff regarding the payment proccess.
                            <script> setTimeout(function (){ window.top.location.href = "<?php echo UrlHelper::siteUrl(); ?>success/<?php echo $ageGroup; ?>?n=<?php echo base64_encode($n); ?>";, 5000);</script>
                            </body>
                            </html>
                            <?php
                            break;
                        case 9: // Success
                            // Redirect to success page!
                            $n = "entry=".$paramsPlus['ENTRYID']."&ord=".$returndata['Alias.OrderId'];
                            if(isset($paramsPlus['USERPROMOID'])){
                                $n .= "&userPromoId=".$paramsPlus['USERPROMOID'];
                            }
                            $ageGroup = $paramsPlus['AGEGROUP'] ? $paramsPlus['AGEGROUP']."/": "";
                            EF::log('Payment successful');
                            ?>
                            <html>
                            <body class="--theme-bg--default">
                            Please wait while we redirect you.
                            <script> setTimeout(function (){ window.top.location.href = "<?php echo UrlHelper::siteUrl(); ?>success/<?php echo $ageGroup; ?>?n=<?php echo base64_encode($n); ?>";, 3000);</script>
                            </body>
                            </html>
                            <?php
                            break;

                    }

                    if($errorMsg) {
//                        If we got any of the specific errors (1 to 4), lets log it and send the user to the success page,
//                        since we got the reservation anyway.

                        $n = "entry=".$paramsPlus['ENTRYID']."&ord=".$returndata['Alias.OrderId'];
                        if(isset($paramsPlus['USERPROMOID'])){
                            $n .= "&userPromoId=".$paramsPlus['USERPROMOID'];
                        }
                        $ageGroup = $paramsPlus['AGEGROUP'] ? $paramsPlus['AGEGROUP']."/": "";
                        EF::log('A problem occurred, we\'ve redirected the user to the success page, but the payment didn\' go through. '.$errorMsg);
                        ?>
                        <html>
                        <body class="--theme-bg--default">
                        A problem occurred during the payment, you will be contacted by a member of our staff regarding the payment proccess.
                        <script> setTimeout(function (){ window.top.location.href = "<?php echo UrlHelper::siteUrl(); ?>success/<?php echo $ageGroup; ?>?n=<?php echo base64_encode($n); ?>"};, 3000);</script>
                        </body>
                        </html>
                        <?php
                    }

                }catch (Exception $e){
                    return false;
                }

                break;
            case '1': // Not Ok
                // Send user back to payment?
                // redirect to payment with WEBFORMSUMISSIONID
                $redirect_entry = Craft::$app->elements->getElementById($paramsPlus["ENTRYID"]);
                EF::log('Payment canceled for user. ');
                ?>
                <html>
                <body class="--theme-bg--default">
                <div>Payment failed, you haven't been charged, but your reservation is saved.<br/><br/></div>
                <div>You will be contacted soon.<br/><br/></div>
                <div>You will be redirected to the <a href="<?php echo $redirect_entry->url; ?>"><?php echo $redirect_entry->title ?></a> destination page.<br/><br/></div>
                <script> setTimeout(function (){ window.top.location.href = "<?php echo $redirect_entry->url; ?>"}, 5000);</script>
                </body>
                </html>
                <?php
                break;
            case '2': // Alias Updated
                break;
            case '3': // Cancelled by user
                $redirect_entry = Craft::$app->elements->getElementById($paramsPlus["ENTRYID"]);
                EF::log('Payment canceled by user. ');
                ?>
                <html>
                <body class="--theme-bg--default">
                You cancelled the payment, you will be redirected to the <a href="<?php echo $redirect_entry->url; ?>"><?php echo $redirect_entry->title ?></a> destination page
                <script> setTimeout(function (){window.top.location.href = "<?php echo $redirect_entry->url; ?>"}, 2000);</script>
                </body>
                </html>
                <?php
                break;
            default:

        }
        exit();
    }

}
