<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\services;

use ef\efcourses\EfCourses;

use Craft;
use craft\elements\Entry;
use craft\base\Component;

use GuzzleHttp\Client;
use ef\efcourses\records\Country;
use ef\efcourses\records\City;
use ef\efcourses\records\CityField;
use ef\efcourses\records\ProductCode;
use ef\efcourses\records\ProductCodeDetail;
use ef\efcourses\records\CourseType;
use ef\efcourses\records\CourseTypeField;
use ef\efcourses\records\Course;

/**
 * EfCoursesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class EfCoursesILS extends Component
{
    public $productCodes = [
        [
            'productName' => 'ILS',
            'productCode' => 'ils',
        ],
    ];

    private $domain = "https://qa.ef.fi";
    private $productObj = [];
    private $currentProduct = null;
    private $status = [];
    private $validLocations = ['Madrid', 'Helsinki'];

    // Public Methods
    // =========================================================================

    public function init()
    {
        parent::init();

        $this->productObj = $this->checkProductCode();

        if(count($this->productObj) < 2){
            $this->currentProduct = $this->productObj[0];
        }
    }

    private function checkProductCode()
    {
        $PCEntries = [];

        // Check if all product codes are saved
        foreach($this->productCodes as $productCode){
            $productObj = ProductCode::find()
                ->where(['productName' => $productCode['productName']])
                ->andWhere(['productCode' => $productCode['productCode']])
                ->with('details')
                ->one();

            if ($productObj === null) {
                $productObj = new ProductCode();
                $productObj->productName = $productCode['productName'];
                $productObj->productCode = $productCode['productCode'];
                $productObj->save();
            }

            if( isset($productCode['productCodeDetail'])) {
                // Check for the extras
                foreach ($productCode['productCodeDetail'] as $pExtra) {
                    $extraObj = ProductCodeDetail::find()
                        ->where(['productCodeId' => $productObj->id])
                        ->andWhere(['productCodeDetail' => $pExtra])
                        ->one();

                    if ($extraObj === null) {
                        $extraObj = new ProductCodeDetail();
                        $extraObj->productCodeDetail = $pExtra;
                        $extraObj->link('productCode', $productObj);
                    }
                }
            }
            $PCEntries[] = $productObj;
        }

        return $PCEntries;
    }

    private function fetchRequest($reqUri)
    {
        $client = new \GuzzleHttp\Client();

        try{
            // $request = $client->createRequest('GET', $reqUri,[]);
            $response = $client->request('GET', $reqUri,[]);

            // $response = $request->send();

        }catch (Exception $e){
            Craft::getLogger()->log("There was an error with Curl", Craft::getLogger()::LEVEL_ERROR, 'EF Courses');
            return false;
        }

        return json_decode($response->getBody());
    }

    public function getProductCodeIds()
    {
        $pids = [];

        foreach ($this->productObj as $p) {
            $pids[] = $p->id;
        }

        return $pids;
    }
    public function fetchLocations()
    {
        foreach ($this->productObj as $product) {
            $this->currentProduct = $product;
            $destinations = $this->fetchRequest($this->domain.'/ef-services/reservationservice/booking/getbookingdestinations/?marketcode=FI&programcode=ILS&productCode=LS');

            if(!is_array($destinations)){
                return false;
            }

            $lastCountry = null;

            foreach($destinations as $country) {
                $countryObj = $this->saveCountry($country);

                foreach ($country->CityList as $city) {
                    $cityObj = $this->saveCity($city, $countryObj);
                }
            }
        }

        return $this->status;
    }

    public function saveCountry($dest)
    {
        // Check if the Country exists, if not, save it.
        $country = Country::find()
            ->where(['countryName' => $dest->CountryName])
            ->one();

        if ($country === null) {
            $country = new Country();
            $country->countryName = $dest->CountryName;
            $country->countryValue = $dest->CountryValue;
            $country->save();

            $this->addStatus('newCountry');
        } else {
            if ($country->countryValue !== $dest->CountryValue) {
                $country->countryValue = $dest->CountryValue;
                $country->save();

                $this->addStatus('updatedCountry');
            }
        }

        return $country;
    }

    public function saveCity($dest, $country)
    {
        // Check if the city exists, and if not, save it (both name and codes)
        $city = City::find()
            ->where(['cityName' => $dest->CityName])
            ->andWhere(['countryId' => $country->id])
            ->with('cityFields')
            ->one();

        if ($city === null) {
            $city = new City();
            $city->countryId = $country->id;
            $city->cityName = $dest->CityName;
            $city->link('country', $country);
            $this->addStatus('newCity');
        }

        // Check for the fields
        // First check if this city already has fields (the field might have changed and we need to update
            // TODO: Do it in LT also
        // If there are no fields add them
        // If there are, and are different, disable the old one's and add the new

        $cityField = CityField::find()
            ->where(['cityId' => $city->id])
            ->andWhere(['productCodeId' => $this->currentProduct->id])
            ->andWhere(['value' => $dest->CountryCode."|".$dest->CityValue])
            ->one();

        $this->addStatus('checkCityField');

        if ($cityField === null) {
            // Check if we have any fields with CityValue, that we need to disable
            $cityField = CityField::find()
                ->where(['valueName' => 'CityValue'])
                ->andWhere(['cityId' => $city->id])
                ->andWhere(['enable' => true])
                ->one();

            if ($cityField !== null) {
                $cityField->enable = false;
                $cityField->save();
                $this->addStatus('disabledCityField');
            }

            // Now lets save the new value
            $cityFieldNew = new CityField();
            $cityFieldNew->cityId = $city->id;
            $cityFieldNew->valueName = 'CityValue';
            $cityFieldNew->value = $dest->CountryCode."|".$dest->CityValue;
            $cityFieldNew->enable = true;
            $cityFieldNew->link('product', $this->currentProduct);
            $this->addStatus('addCityField');

            // TODO: Check if there are multiple fields for this city (shouldn't)

            // TODO: Some cities have more than one value, we need to figure out the best way to fix it!
        }

        return $city;
    }

    public function fetchCourses()
    {
        foreach ($this->productObj as $product){
            $this->currentProduct = $product;

            // Get all cities
            $cities = CityField::find()
                ->where(['productCodeId' => $product->id])
                ->andWhere(['enable' => true])
                ->with('city')
                ->all();

            foreach ($cities as $cityField){
                $result = $this->fetchCity($cityField);
            }
        }

        if (!count($this->status)){
            return ["msg" => "No changes were made!"];
        }

        return $this->status;
    }

    public function fetchCity($cityField){
        $courses = $this->fetchRequest($this->domain.'/ef-services/reservationservice/booking/GetILSCreativePrices/?marketcode=FI&ProductCode=LS&programcode=ILS&destinationcode='.$cityField->value.'&mc=FI');

        foreach ($courses as $course){

            // Get/Save CourseType
            $courseType = $this->saveCourseType($course);

            // If the course is not enabled let's skip the course saving
            if (!$courseType->enable) {
                continue;
            }

            // Get/Save Course

            $course = $this->saveCourse($course, $courseType, $cityField->city);
        }

        return $this->status;

    }

    public function saveCourseType($course)
    {
        // Check if the courseType exists
        $courseType = CourseType::find()
            ->where(['courseType' => $course->Coursenumber])
            ->one();

        if ($courseType === null) {
            $courseType = new CourseType();
            $courseType->courseType = $course->Coursenumber;
            $courseType->link('product', $this->currentProduct);
            $this->addStatus('newCourseType');

            // Save all fields
            $fields = ['MinLength', 'MaxLength', 'Coursename', 'CourseDescription', 'SortOrder'];

            foreach ($fields as $field) {
                $courseTypeField = new CourseTypeField();
                $courseTypeField->valueName = $field;
                $courseTypeField->value = trim($course->{$field});

                $courseTypeField->link('courseType', $courseType);
            }
        }

        return $courseType;
    }

    public function saveCourse($course, $courseType, $city)
    {
        $courseObj = Course::find()
            ->where(['cityId' => $city->id, 'courseTypeId' => $courseType->id])
            ->one();

        // If exists and the price is the same do nothing
        if($courseObj !== null && ($courseObj->minPrice === (int)$course->MinPriceforMinLength))
        {
            $this->addStatus('skippedCourse');
            return $courseObj;
        }

        if($courseObj === null)
        {
            $courseObj = new Course;
            $this->addStatus('newCourse');
        } else {
            $this->addStatus('updatedCourse');
        }

        $courseObj->cityId = $city->id;
        $courseObj->courseNumber = 0;
        $courseObj->startDate = null;
        $courseObj->minPrice = $course->MinPriceforMinLength;
        $courseObj->duration = $course->MinLength;
        $courseObj->courseContent = json_encode($course);
        $courseObj->link('courseType', $courseType);

        //TODO: finish add info to course, find out best option to update info if needed.

        return $courseObj;
    }

    private function addStatus($status)
    {
        if (!isset($this->status[$status])) {
            $this->status[$status] = 0;
        }

        $this->status[$status]++;
    }

    public function toggleCourseType($id, $value)
    {
        $courseType = CourseType::find()
            ->where(['id' => $id])
            ->one();

        if ($courseType === null) {
           return false;
        }

        $value = ($value === 'false') ? false : true;

        $courseType->enable = $value;
        if ($courseType->save()) {
            return true;
        }

        return false;
    }

    public function updateLocationCourses($uses)
    {
        $coursesCities = [];

        foreach($uses as $use){
            $coursesCities[$use['value']][] = $use['id'];
        }



        foreach($coursesCities as $value => $ids) {
            $upd = Craft::$app->db->createCommand()
                ->update('ef_courses', ['enable' => $value], ['in', 'id', $ids])
                ->execute();
        }

        return "Courses Updated";
    }

    public function fetchCities()
    {
        $cities = CityField::find()
            ->where(['productCodeId' => $this->currentProduct->id])
            ->with('city')
            ->all();

        return $cities;
    }

    // TODO: check this function for MyTrips
    public function getCoursesFrontend($associative = false, $assocKey = 'Coursenumber', $entryId = false){
        $productEntry = $this->checkProductCode();

        $productEntry = $productEntry[0];

        $retArr = array();

        // If a entry ID is passed we need to check what city that entry belongs to, to filter the courses below.
        if($entryId){
            $entryObj = Entry::find()
                ->id($entryId)
                ->one();
        }

        $cityId = unserialize($entryObj->destinations[2]->efCity);
        // If a entry was passed lets fetch the course types based on the city of the entry
        if(isset($entryObj) && isset($cityId[0])){

            $courses = CourseType::find()
                ->with(['courses'=> function($query) use ($cityId){
                        $query->andWhere(['cityId' => $cityId]);
                    },
                'fields'])
                ->where(['enable' => '1', 'productCodeId' => $productEntry->id])
                ->all();

        } else {
            $courses = CourseType::find()
                ->with('fields')
                ->where(['productCodeId' => $productEntry->id])
                ->all();

        }

        $coursesArr = [];
        foreach($courses as $course){
            $courseArr = [];
            $courseArr['Coursenumber'] = $course->courseType;
            $courseArr['CourseIDLocal'] = $course->id;

            // Fetch all other API Course Information
            foreach($course->fields as $courseField){
                $courseArr[$courseField->valueName] = $courseField->value;
            }

            // Get the Course Entry that this course relates to
            $entries = Entry::find()
                ->section('pages')
                ->andWhere(['IN', 'field_efCourse', $course->id])
                ->all();

            if(!empty($entries)){
                $courseArr['id'] = $entries[0]['id'];
                $courseArr['CourseNameLabel'] = $entries[0]['field_CourseNameLabel'];
                $courseArr['CourseDescriptionLabel'] = $entries[0]['field_CourseDescriptionLabel']!== '' ? $entries[0]['field_CourseDescriptionLabel'] : $entries[0]['field_numberLessons'].' '.Craft::t('lessons/week') ;
                $courseArr['defaultSelected'] = $entries[0]['field_defaultSelected'] ? true: false;
                $courseArr['hasRibbon'] = $entries[0]['field_isPopular'] ? true: false;
                $courseArr['ribbonLabel'] = Craft::t('Popular');

                // TODO: change this after the Course Entry Type is working
            }

            if ($associative){
                $coursesArr[$courseArr[$assocKey]] = $courseArr;
            }else{
                $coursesArr[] = $courseArr;
            }
        }

        return $coursesArr;
    }
}
