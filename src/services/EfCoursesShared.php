<?php
/**
 * EF Courses plugin for Craft CMS 3.x
 *
 * Plugin to scrape EF Courses to use on the website
 *
 * @link      https://ef.design
 * @copyright Copyright (c) 2019 EF Global Creative
 */

namespace ef\efcourses\services;

use ef\efcourses\EfCourses as EF;

use Craft;
use craft\base\Component;
use craft\elements\Category as Category;
use craft\elements\Entry as Entry;
use modules\ccsutilsmodule\CcsUtilsModule as EFUtils;


use GuzzleHttp\Client;
use ef\efcourses\records\Country;
use ef\efcourses\records\City;
use ef\efcourses\records\CityField;
use ef\efcourses\records\ProductCode;
use ef\efcourses\records\ProductCodeDetail;
use ef\efcourses\records\CourseType;
use ef\efcourses\records\CourseTypeField;
use ef\efcourses\records\Course;
use ef\efcourses\records\Market;

/**
 * EfCoursesService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    EF Global Creative
 * @package   EfCourses
 * @since     0.0.1
 */
class EfCoursesShared extends Component
{

    private $domain = "https://qa.ef.fi";
    private $productObj = [];
    private $currentProduct = null;
    private $status = [];
    private $validLocations = ['Madrid', 'Helsinki'];
    private $calculateExtras = true;
    private $showMinPriceinWeeks = false;

    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     EfCourses::$plugin->efCoursesService->exampleService()
     *
     * @return mixed
     */

    public function init()
    {
        parent::init();

    }

    public function getProductList()
    {
        $productArray = [];

        $products = ProductCode::find()
            ->all();

        foreach ($products as $prod) {
            $productArray[$prod->id] = $prod->productName;
        }

        return $productArray;
    }

    public function flatten(array $array)
    {
        $return = array();
        array_walk_recursive($array, function ($a) use (&$return) {
            $return[] = $a;
        });
        return $return;
    }


    public function getCityList()
    {
        $cityArray = [];

        $cities = City::find()
            ->with('country', 'cityFields')
            ->all();

        foreach ($cities as $city) {
            $cityArray[$city->id] = $city->cityName . ', ' . trim($city->country->countryName);
        }

        return $cityArray;
    }

    public function getCoursesList()
    {
        $coursesArray = [];

        $courses = CourseType::find()
            ->where('enable = 1')
            ->with('fields', 'product')
            ->all();

        foreach ($courses as $course) {
            $courseName = "";
            foreach ($course->fields as $field) {
                if (in_array($field->valueName, ['Coursename', 'CourseType'])) {
                    $courseName = $field->value;
                }
            }

            $coursesArray[$course->id] = $courseName . ' - ' . $course->courseType;
        }

        return $coursesArray;
    }

    public function updateDestinationAssociations($locations)
    {
        foreach ($locations as $cityId => $catId) {
            $category = Category::find()
                ->group('destinations')
                ->level('3')
                ->id($catId)
                ->one();

            if ($category !== null) {
                // Check if efCity has a value
                if (isset($category->efCity)) {
                    $efCity = unserialize($category->efCity);
                    if (is_array($efCity)) {
                        if (!in_array($cityId, $efCity)) {
                            $category->efCity = [$cityId];
                            Craft::$app->getElements()->saveElement($category);
                        }
                    } else {
                        $category->efCity = [$cityId];
                        Craft::$app->getElements()->saveElement($category);
                    }
                }
            }

        }
    }

    public function getDestinationMinPrice($entry, $promotions = false)
    {

        // Check if we need to fetch the parent instead
        if (strcmp($entry->type->name, 'Destination') !== 0) {
            $entry = $entry->parent;
        };

        // Fetch city category for the entry
        $dest = $entry->destinations->level(3)->one();

//        Seems to not be needed now, return if needed
//        if (count($entry->destinations[2])) === 1) {
//        } else {
//            // TODO: What should be done if there are more categories? Currently there can't be more
//            return '';
//        }


        // Array with list of API cities where this entry belongs to
        $cityArr = unserialize($dest->efCity);

        // Product Code of this entry based on the age Group it belongs to
        if (!isset($entry->ageGroups[0]->efProducts)) {
            return false;
        }
        $productCodeID = $entry->ageGroups[0]->efProducts;

        $destEntry = $entry;

        // Check if we need to calculate another
        // Get minimum price and duration, based on a city and a product code

        $query = (new \craft\db\Query());
        $query->select('cc.id, cityId, productCode, cityName, courseContent, minprice as price, duration');
        $query->from('ef_courses cc');
        $query->join('LEFT JOIN', 'ef_cities c', 'cc.cityId = c.id');
        //$query->join('ef_cities c', 'cc.cityId = c.id');
        $query->join('LEFT JOIN', 'ef_coursetypes ct', 'ct.id = cc.coursetypeId');
        //$query->join('ef_coursetypes ct', 'ct.id = cc.coursetypeId');
        $query->join('LEFT JOIN', 'ef_productcodes pc', 'ct.productCodeId = pc.id');
//        $query->join('ef_productcodes pc', 'ct.productCodeId = pc.id');
        $query->where('pc.id = :productCode and cc.enable = 1', array(':productCode' => $productCodeID));
        $query->andWhere(array('in', 'c.id', $cityArr));
        $query->orderBy('price asc');
        $entries = $query->all();

        $entryMin = &$entries[0];

        if ($entryMin === NULL) {
            return false;
        }

        $entryContent = json_decode($entryMin['courseContent']);

        $extras = 0;
        // Check if we have extras to calculate
        if ($this->calculateExtras && isset($entryContent->MaterialsPrice)) {
            $extras = $entryContent->AppFeePrice + $entryContent->MaterialsPrice + $entryContent->TravelInsurancePrice;
        }

        $durationRange = [];

        foreach ($entries as &$c) {
            if (strpos($c['productCode'], 'lt') !== false) {
                $minDur = round($c['duration'] / 7);
            } else {
                if ($c['duration'] === "1") {
                    $minDur = 2;
                } else {
                    $minDur = intval($c['duration'], 10);
                }
            }

            $c['weekDuration'] = $minDur;

            if (!in_array($minDur, $durationRange)) {
                $durationRange[] = $minDur;
            }
        }

        $minDur = min($durationRange);

        // Get Promotions for this entry if they have not been passed on function call
        if (!$promotions) {
            $promotions = EFUtils::$instance->CcsUtilsService->getPromotionsForEntry($destEntry->id);
            //$promotions = craft()->ccsUtils->getPromotionsForEntry($destEntry->id);
        }

        $minPromos = [];
        foreach ($promotions as $promo) {
            $minPromos[$promo->numberOfWeeks] = $promo;
        }
        // Check if there is a promo with the minDur
        $minPromo = 0;
        if ($promotions) {
            $promo = array_filter($promotions,
                function ($promotion) use ($minDur, $entryContent) {
                    if (isset($entryContent->CourseNumber) && $promotion->courseNumbers !== "") {
                        $courseNumbers = explode(",", $promotion->courseNumbers);
                        if (in_array($entryContent->CourseNumber, $courseNumbers)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    if (($promotion->numberOfWeeks <= $minDur && $promotion->maxWeeks >= $minDur) || ($promotion->numberOfWeeks >= $minDur && $promotion->maxWeeks === 0)) {
                        return true;
                    }
                    return false;
                });
            $promo = array_values($promo);
            if (count($promo) > 0) {
                $minPromo = $promo[0];
            }
        }
        // TODO: Check why the pricing on the discount is not showing

        // Initialize the return array
        $infoToUpdate = [
            'from' => Craft::t("site", "from"),
            'valueFormat' => '',
            'value' => '',
            'minPrice' => null,
            'theme' => ''
        ];

        // Set the values array we're going to use for calculations
        $values =
            ['price' => $entryMin['price'],
                'extras' => $extras,
                'weeks' => $entryMin['weekDuration'],
                'pricePerWeek' => $entryMin['price'] / $entryMin['weekDuration'],
            ];

        if ($minPromo) {
            $addExtras = 0;
            if (strcmp($minPromo->discountAppliedTo, 'basePrice') === 0) {
                $pFP = $values['price']; // Price For Promo
                $addExtras = $values['extras'];
            } else {
                $pFP = $values['price'] + $values['extras'];
            }

            // if it's per week, apply deductible to the $value
            if (strcmp($minPromo->typeOfPromotion, 'fixedAmountPerWeek') === 0) {
                $values['promoPrice'] = ((($pFP / $values['weeks']) - $minPromo->promotionDeductible)) * $values['weeks'];
            } else if (strcmp($minPromo->typeOfPromotion, 'percentage') === 0) {
                // if it's percentage, apply a percentage
                $values['promoPrice'] = (($pFP - ($pFP * ($minPromo->promotionDeductible / 100))));
            } else {
                // else apply the value/week
                $values['promoPrice'] = ($pFP - $minPromo->promotionDeductible);
            }

            // Add $addExtras, that should be 0 if they were added before, or have a value,
            // if the promo calculations was just for the basePrice

            $values['promoPrice'] += $addExtras;
        }

        // Set the price per Week or total
        if (strcmp($entryMin['productCode'], 'ils') === 0 && $this->showMinPriceinWeeks) {
            $values['showPrice'] = ($values['price'] + $values['extras']) / $values['weeks'];
            if (isset($values['promoPrice'])) {
                $values['showPromoPrice'] = $values['promoPrice'] / $values['weeks'];
            }
            $values['valueFormat'] = '€{value}/w';
        } else {
            $values['showPrice'] = $values['price'] + $values['extras'];
            if (isset($values['promoPrice'])) {
                $values['showPromoPrice'] = $values['promoPrice'];
            }
            $values['valueFormat'] = '€{value}';
        }

        // Set the return array with all the correct values
        if ($entryMin['price'] > 0 && $minPromo) {
            $infoToUpdate['valueFormat'] = $values['valueFormat'];
            $infoToUpdate['value'] = $values['showPrice'];
            $infoToUpdate['promoValue'] = $values['showPromoPrice'];
            $infoToUpdate['theme'] = $minPromo->colorTheme->value;
        } else if (count($minPromos) > 0) {
            $infoToUpdate['valueFormat'] = $values['valueFormat'];
            $infoToUpdate['value'] = $values['showPrice'];
            $infoToUpdate['promoText'] = craft::t("site", "Discount");
            $infoToUpdate['theme'] = "yellow";
        } else {
            $infoToUpdate['valueFormat'] = $values['valueFormat'];
            $infoToUpdate['value'] = $values['showPrice'];
        }

        $infoToUpdate['minPrice'] = $infoToUpdate['value'];
        $infoToUpdate['value'] = Craft::t("site", $infoToUpdate['valueFormat'], array('value' => round($infoToUpdate['value'])));
        if (isset($infoToUpdate['promoValue'])) {
            $infoToUpdate['promoValue'] = Craft::t("site", $infoToUpdate['valueFormat'], array('value' => round($infoToUpdate['promoValue'])));
        } else {
        }
        return $infoToUpdate;
    }

    public function getCourseType($entry)
    {
        // TODO: After CourseTypes are saved, fetch the coursetype code list (for now only needed for LT)
        if (strcmp($entry->type->name, 'Destination') !== 0) {
            $entry = $entry->parent;
        };

        $productCodeID = $entry->ageGroups[0]->efProducts;

        $cityID = unserialize($entry->destinations->level(3)[0]->efCity);

        $courseTypes = CourseType::find()
            ->select('courseType')
            ->joinWith('courses')
            ->where('productCodeID = :codeId', [':codeId' => $productCodeID])
            ->andWhere(['IN', 'cityId', $cityID])
            ->all();

        $courseArr = [];

        foreach ($courseTypes as $courseType) {
            if (!in_array($courseType->courseType, $courseArr)) {
                $courseArr[] = $courseType->courseType;
            }
        }

        return implode("|", $courseArr);

        /*if(count($entry->destinations->level(3)) === 1){
            $dest = $entry->destinations->level(3)[0];
        }else{
            // TODO: What should be done if there are more categories? Currently there can't be more
        }*/


    }

    public function getDestinationCode($entry, $simpleCodes = false)
    {
        $dest = null;
        $destinationValues = [];

        if (!$entry) {
            return false;
        }

        if (strcmp($entry->type->name, 'Destination') !== 0) {
            $entry = $entry->parent;
        };

        // Get the city category
        if (isset($entry->destinations) && count($entry->destinations->level(3)) === 1) {
            $dest = $entry->destinations->level(3)->one();
        } else {
            // TODO: What should be done if there are more categories? Currently there can't be more
            return false;
        }

        // Array with list of API cities where this entry belongs to
        $cityArr = unserialize($dest->efCity);

        if (!is_array($cityArr) || empty($cityArr)) {
            return false;
        }

        // Product Code of this entry based on the age Group it belongs to
        $productCodeID = $entry->ageGroups[0]->efProducts;

        // Get City Code value from the database, based on PRODUCT CODE and CITY
        $cityFields = CityField::find()
            ->select('productCodeId, value')
            ->where('productCodeId = :productCodeID and enable = 1', [':productCodeID' => $productCodeID])
            ->andWhere(['IN', 'cityId', $cityArr])
            ->with('product')
            ->all();

        // TODO: Test with all types of courses
        foreach ($cityFields as $cityField) {
            if ($simpleCodes && $cityField->product->productName == 'ILS') {
                $cityExploded = explode('|', $cityField->value);
                $destinationValues[] = $cityExploded[1];
            } else if ($simpleCodes && $cityField->product->productName !== 'ILS') {
                $cityExploded = explode(',', $cityField->value);
                foreach ($cityExploded as $cityEx) {
                    $citiesCode[] = explode("|", $cityEx)[0];
                }
                $destinationValues[] = implode('|', $citiesCode);
            } else {
                $destinationValues[] = $cityField->value;
            }
        }

        return implode(',', $destinationValues);
    }

    public function getProductId($productCode, $productName = false)
    {
// LT
        $pcQuery = [];

        if ($productName) {
            $pcQuery = ['like', 'productName', $productName];
        } else {
            $pcQuery = ['like', 'productCode', $productCode];
        }

        $courseTypes = ProductCode::find()
            ->where($pcQuery)
            ->all();

        $courseTypesArr = [];

        if (count($courseTypes) < 1) {
            return [];
        }

        if (count($courseTypes) < 2) {
            $courseTypesArr = array((int)$courseTypes[0]->id);
        } else {
            foreach ($courseTypes as $courseType) {
                $courseTypesArr[] = (int)$courseType->id;
            }
        }

        return $courseTypesArr;

    }

    function getDestinationsJSON($productCode = false)
    {
        $pc = [];
        $destJSON = [];
        $citiesJSON = [];
        $lastCountry = false;
        $countryPos = 0;

        if ($productCode) {
            $pc = $this->getProductId($productCode);
        }

        $validCitiesIDs = $this->getValidCitiesIDs($productCode);

        $cities = City::find()
            ->joinWith([
                'cityFields cf' => function ($query) {
                    $query->andWhere(['IN', 'cf.cityId', [8]]);
                },
            ])
            ->joinWith('country');

        if (!empty($pc)) {
            $cities->andWhere(['IN', 'productCodeId', $pc]);
        }

        $cities = $cities->all();

        foreach ($cities as $city) {
            if ($lastCountry && $lastCountry !== $city->country->countryCode) {
                $destJSON[$countryPos]['cities'] = $citiesJSON;
                $citiesJSON = [];
                $countryPos++;
            }
            if ($lastCountry === false || $lastCountry !== $city->country->countryCode) {
                $destJSON[$countryPos]['countryName'] = $city->country->countryName;
                $destJSON[$countryPos]['countryCode'] = $city->country->countryCode;
            }

            if (strcmp($productCode, 'ils') === 0) {
                $cityL = explode("|", $city->cityFields[0]->value)[1];
            } else {
                $cityLocs = explode(",", $city->cityFields[0]->value);
                $cityL = [];
                foreach ($cityLocs as $cityLoc) {
                    $cityL[] = explode("|", $cityLoc)[0];
                }
                $cityL = implode("|", $cityL);
            }

            $cityJSON = ['cityName' => $city->cityName,
                'cityCode' => $cityL];

            $citiesJSON[] = $cityJSON;
            $lastCountry = $city->country->countryCode;
        }
        $destJSON[$countryPos]['cities'] = $citiesJSON;

        return $destJSON;
    }

    public function getValidCitiesIDs($productCode = false)
    {

        // Fetch category of Product
        $product = Category::find()
            ->group('products')
            ->title(strtoupper($productCode))
            ->one();

        // Fetch destinations (related to $productCode)
        $parents = Entry::find()
            ->section('pages')
            ->relatedTo($product)
            ->all();

        // Fetch all destinations in category, related to the destinations fetched previously
        $destinationsCat = Category::find()
            ->group('destinations')
            ->level(3)
            ->relatedTo($parents)
            ->all();

        $cityIDs = [];

        foreach ($destinationsCat as $p) {
            $EFcities = unserialize($p->efCity);
            foreach ($EFcities as $c) {
                $cityIDs[] = $c;
            }
        }

        return array_unique($cityIDs);
    }

    public function getDestinationCourses($entry)
    {
        if (strcmp($entry->type->name, 'Destination') !== 0) {
            // TODO: Could find the first Destination page after the Age Filter in case of more than 3 pages
            $entry = $entry->parent;
        };

        // Fetch city category for the entry
        if (count($entry->destinations->level(3)) === 1) {
            $dest = $entry->destinations->level(3)->one();
        } else {
            // TODO: What should be done if there are more categories? Currently there can't be more
        }

        // Array with list of API cities where this entry belongs to
        $cityArr = unserialize($dest->efCity);

        // Product Code of this entry based on the age Group it belongs to
        $ageGroup = $entry->ageGroups->level(1)->one();
        if (!isset($ageGroup->efProducts)) {
            return false;
        }
        $productCodeID = $ageGroup->efProducts;

        // Get courseType from citiescourses table
        $query = (new \craft\db\Query());
        $query->select('ct.id')->distinct();
        $query->from('ef_courses cc');
        $query->join('LEFT JOIN', 'ef_coursetypes ct', 'ct.id = cc.courseTypeId');
        $query->join('LEFT JOIN', 'ef_productcodes pc', 'ct.productCodeId = pc.id');
        //$query->andWhere('cc.cityId = :cityId', array(':cityId' => $cityID));
        $query->andWhere('ct.productcodeId = :productCodeId and cc.enable = 1', array(':productCodeId' => $productCodeID));

        /// Create custom WHERE with OR between entries, but add AND at the beginning
        $andWhere = [];
        foreach ($cityArr as $cityAr) {
            $andWhere[] = "cc.cityId = '" . $cityAr . "'";
        }
        $andWhereSTR = implode(' OR ', $andWhere);

        $where = $query->where;
        $where = $where . ' AND (' . $andWhereSTR . ')';
        $query->where = $where;


        $courseType = $query->all();

        $courseType = $this->flatten($courseType);

        // Get all Entries
        $courses = Entry::find()
            ->select('elements.id as id')
            ->where(['IN', 'field_efCourse', $courseType])
            ->asArray()
            ->all();

        $coursesid = $this->flatten($courses);

        return $coursesid;
    }

    public function getAvailableLocations($entry)
    {
        if (!isset($entry->efCourse)) {
            return false;
        }
        $courseID = $entry->efCourse;

        if (!$entry->efCourse) {
            return false;
        }

        $query = (new \craft\db\Query());
        $query->select('c.id')->distinct();
        $query->from('ef_courses cc');
        $query->join('LEFT JOIN', 'ef_cities c', 'cc.cityId = c.id');
        $query->where('cc.courseTypeId = :ctid and cc.enable = 1', array(':ctid' => $courseID));
        $query->orderBy('c.id asc');
        // TODO: and where CourseType -> toUse = 1
        $cityCourseIDs = $query->all();

        // If there are no location with this course, return now
        if (empty($cityCourseIDs)) {
            return false;
        }

        $cityCourseIDs = $this->flatten($cityCourseIDs);

        // Get all Categories
        $cities = Category::find()
            ->andWhere('field_efCity IS NOT NULL');

        /// Create custom WHERE with OR between entries, but add AND at the beginning
        $andWhere = [];
        foreach($cityCourseIDs as $cityCourseID){
            $andWhere[] = "field_efCity LIKE '%\"".$cityCourseID."\"%'";
        }
        $andWhereSTR = implode(' OR ',$andWhere);

        $where = $cities->where;
        $where = $where .' AND ('.$andWhereSTR.')';
        $cities->where = $where;

        $cities = $cities->all();

        return $cities;

        return CategoryModel::populateModels($cities);
    }

    public function saveMarket($marketObj)
    {
        $market = new Market();
        $market->siteId = $marketObj['siteId'];
        $market->marketCode = $marketObj['marketCode'];
        $market->mainLocation = $marketObj['mainLocation'];

        return $market->save();
    }

    /*
     * Checks if the current status of the market list is correct
     *
     * Ex: If we have one market entry for each site
     *
     */

    public function getMarketsChecks()
    {
        $sitesIds = [];

        // Get all sites ids
        foreach(Craft::$app->sites->getAllSites() as $site){
            $sitesIds[] = $site->id;
        }

        $marketsNumber = (int) Market::find()
            ->where(['IN', 'siteId', $sitesIds])
            ->count();

        return count($sitesIds) === $marketsNumber;
    }
}
